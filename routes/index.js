const express = require("express");
const router = express.Router();
const nodemailer = require("nodemailer");
require("dotenv").config();

router.get("/", function (req, res, next) {
  res.send("Welcome to mail-express");
});

router.post("/send-email", async (req, res) => {
  try {
    let transporter = nodemailer.createTransport({
      host: process.env.EMAIL_HOST,
      port: process.env.EMAIL_PORT,
      secure: process.env.EMAIL_PORT == 465 ? true : false, // true for 465, false for other ports
      auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASSWORD,
      },
    });
    let mailOptions = {
      from: req.body.emailSender || process.env.EMAIL_USER,
      to: req.body.emailReceiver,
      subject: req.body.emailSubject,
      html: req.body.content,
    };
    transporter.sendMail(mailOptions, (error, info) => {
      let status = error ? 0 : 1;
      if (info.messageId) status = 1;
      if (status) {
        return res.status(200).json({ message: "Success to send email." });
      }
      return res.status(500).send({
        message: "Fail to send email.",
      });
    });
  } catch (error) {
    return res.status(500).send({
      message: "Fail to send email.",
    });
  }
});

module.exports = router;
