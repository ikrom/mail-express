const nodemailer = require("nodemailer");
require("dotenv").config();
let transporter = nodemailer.createTransport({
  host: process.env.EMAIL_HOST,
  port: process.env.EMAIL_PORT,
  secure: process.env.EMAIL_PORT == 465 ? true : false, // true for 465, false for other ports
  auth: {
    user: process.env.EMAIL_USER,
    pass: process.env.EMAIL_PASSWORD,
  },
});
exports.sendEmail = async (req, res) => {
  try {
    let mailOptions = {
      from: req.body.emailSender || process.env.EMAIL_USER,
      to: req.body.emailReceiver,
      subject: req.body.emailSubject,
      html: req.body.content,
    };
    transporter.sendMail(mailOptions, (error, info) => {
      let status = error ? 0 : 1;
      if (info.messageId) status = 1;
      if (status) {
        return res.status(200).json({ message: "Sent email is success." });
      }
      return res.status(500).send({
        message: "Sent email is failed",
      });
    });
  } catch (error) {
    return res.status(500).send({
      message: "Sent email is failed",
    });
  }
};
